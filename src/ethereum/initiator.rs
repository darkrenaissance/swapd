use crate::{
    ethereum::{swap_creator::SwapCreator, utils, Error},
    protocol::traits::{ContractSwapArgs, HandleCounterpartyKeysReceivedResult, Initiator},
};

use darkfi_serial::async_trait;
use ethers::prelude::*;

use log::info;

/// Implemented on top of the non-initiating chain
///
/// Can probably become an extension trait of an RPC client eventually
pub(crate) trait OtherChainClient {
    fn claim_funds(
        &self,
        our_secret: [u8; 32],
        counterparty_secret: [u8; 32],
    ) -> Result<(), crate::Error>;
}

pub(crate) struct EthInitiator<M: Middleware, C: OtherChainClient> {
    contract: SwapCreator<M>,
    other_chain_client: C,
    secret: [u8; 32],
}

#[allow(dead_code)]
impl<M: Middleware, C: OtherChainClient> EthInitiator<M, C> {
    pub(crate) fn new(contract: SwapCreator<M>, other_chain_client: C, secret: [u8; 32]) -> Self {
        Self { contract, other_chain_client, secret }
    }
}

#[async_trait]
impl<M: Middleware + 'static, C: OtherChainClient + Send + Sync> Initiator for EthInitiator<M, C> {
    async fn handle_counterparty_keys_received(
        &self,
        args: ContractSwapArgs,
    ) -> Result<HandleCounterpartyKeysReceivedResult, crate::Error> {
        let ContractSwapArgs {
            claim_commitment,
            refund_commitment,
            claimer,
            timeout_1,
            timeout_2,
            asset,
            value,
            nonce,
            ..
        } = args;

        // TODO: ERC20 is *not* handled right now
        if asset != Address::zero() {
            return Err(Error::ERC20NotSupported.into());
        }

        let tx = self
            .contract
            .new_swap(
                claim_commitment,
                refund_commitment,
                claimer,
                timeout_1,
                timeout_2,
                asset,
                value,
                nonce,
            )
            .value(value);
        let receipt = tx
            .send()
            .await
            .map_err(|e| Error::FailedToSubmitTransaction("new_swap".to_string(), e.to_string()))?
            .await
            .map_err(|e| Error::FailedToAwaitPendingTransaction("new_swap".to_string(), e))?
            .ok_or_else(|| Error::NoReceipt)?;

        let block_number =
            receipt.block_number.expect("block number must be set in receipt").as_u64();
        let (contract_swap, swap_id) = utils::parse_new_swap_event_from_receipt(receipt)?;

        info!(
            "initiated swap on-chain: contract_swap_id = {}",
            ethers::utils::hex::encode(&swap_id)
        );

        Ok(HandleCounterpartyKeysReceivedResult {
            contract_swap_id: swap_id,
            contract_swap,
            block_number,
        })
    }

    async fn handle_counterparty_funds_locked(
        &self,
        swap: super::swap_creator::Swap,
        swap_id: [u8; 32],
    ) -> Result<(), crate::Error> {
        let tx = self.contract.set_ready(swap);
        let receipt = tx
            .send()
            .await
            .map_err(|e| Error::FailedToSubmitTransaction("set_ready".to_string(), e.to_string()))?
            .await
            .map_err(|e| Error::FailedToAwaitPendingTransaction("set_ready".to_string(), e))?
            .ok_or_else(|| Error::NoReceipt)?;

        if receipt.status != Some(U64::from(1)) {
            return Err(Error::TransactionFailed("set_ready".to_string(), receipt).into());
        }

        info!("contract set to ready, contract_swap_id = {}", ethers::utils::hex::encode(swap_id));
        Ok(())
    }

    async fn handle_counterparty_funds_claimed(
        &self,
        counterparty_secret: [u8; 32],
    ) -> Result<(), crate::Error> {
        self.other_chain_client.claim_funds(self.secret, counterparty_secret)
    }

    async fn handle_should_refund(
        &self,
        swap: super::swap_creator::Swap,
    ) -> Result<(), crate::Error> {
        let tx = self.contract.refund(swap, self.secret);

        let receipt = tx
            .send()
            .await
            .map_err(|e| Error::FailedToSubmitTransaction("refund".to_string(), e.to_string()))?
            .await
            .map_err(|e| Error::FailedToAwaitPendingTransaction("refund".to_string(), e))?
            .ok_or_else(|| Error::NoReceipt)?;

        if receipt.status != Some(U64::from(1)) {
            return Err(Error::TransactionFailed("refund".to_string(), receipt).into());
        }

        Ok(())
    }
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("failed to create drk struct")]
    DrkInitializationFailed(#[source] darkfi::Error),
    #[error("failed to initialize wallet")]
    InitializeWallet(#[source] darkfi::Error),
    #[error("failed to initialize money")]
    InitializeMoney(#[source] drk::WalletDbError),
    #[error("failed to initialize dao")]
    InitializeDao(#[source] drk::WalletDbError),
    #[error("failed to get default secret")]
    DefaultSecret(#[source] darkfi::Error),
    #[error("failed to generate keypair")]
    Keygen(#[source] drk::WalletDbError),
    #[error("failed to get money balance")]
    MoneyBalance(#[source] darkfi::Error),
    #[error("failed to get aliases mapped by token")]
    GetAliasesMappedByToken(#[source] darkfi::Error),
    #[error("failed to build transfer")]
    BuildTransfer(#[source] darkfi::Error),
    #[error("failed to submit transaction")]
    SubmitTransaction(#[source] darkfi::Error),
    #[error("swap keypair does not exist; generate one first")]
    SwapKeypairDoesNotExist,
    #[error("failed to put keypair")]
    PutKeypair(#[source] drk::WalletDbError),
    #[error("failed convert counterparty secret bytes to secret key")]
    CounterpartySecretKeyConversionFailed(#[source] darkfi_sdk::ContractError),
    #[error("listening to Ready event stream failed")]
    ReadyEventStreamFailed,
    #[error("listening to Refunded event stream failed")]
    RefundedEventStreamFailed,
    #[error("middleware error: {0}")]
    MiddlewareError(String),
}

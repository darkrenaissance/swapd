use crate::ethereum::{error::Error, swap_creator::Swap};
use ethers::{
    abi::{Address, ParamType},
    types::{TransactionReceipt, U64},
};

/// Parse a `newSwap` event from a transaction receipt.
///
/// Returns the `Swap` struct and the swap ID.
pub(crate) fn parse_new_swap_event_from_receipt(
    receipt: TransactionReceipt,
) -> Result<(Swap, [u8; 32]), crate::Error> {
    if receipt.status != Some(U64::from(1)) {
        return Err(Error::TransactionFailed("new_swap".to_string(), receipt).into());
    }

    if receipt.logs.len() != 1 {
        return Err(Error::NewSwapUnexpectedLogCount(receipt.logs.len()).into());
    }

    if receipt.logs[0].topics.len() != 1 {
        return Err(Error::NewSwapUnexpectedTopicCount(receipt.logs[0].topics.len()).into());
    }

    let log_data = &receipt.logs[0].data;

    // ABI-unpack log data
    // note: there are other parameters emitted in the log, but we don't care about them
    let mut tokens = ethers::abi::decode(
        &vec![
            ParamType::FixedBytes(32), // swapID
            ParamType::FixedBytes(32), // claimKey
            ParamType::FixedBytes(32), // refundKey
            ParamType::Address,        // claimer
            ParamType::Uint(256),      // timeout_1
            ParamType::Uint(256),      // timeout_2
            ParamType::Address,        // asset
            ParamType::Uint(256),      // value
            ParamType::Uint(256),      // nonce
        ],
        &log_data.0,
    )
    .map_err(|e| Error::NewSwapLogDecodingFailed(e))?;

    if tokens.len() != 9 {
        return Err(Error::NewSwapUnexpectedLogTokenCount(tokens.len()).into());
    }

    let swap_id = match tokens.remove(0) {
        ethers::abi::Token::FixedBytes(bytes) => {
            // this shouldn't happen, would be an error in ethers-rs
            if bytes.len() != 32 {
                return Err(Error::FixedBytesDecodingError(bytes.len()).into());
            }

            let mut swap_id = [0u8; 32];
            swap_id.copy_from_slice(&bytes);
            swap_id
        }
        token => {
            return Err(Error::ExpectedFixedBytes(token).into());
        }
    };
    let claim_commitment = match tokens.remove(0) {
        ethers::abi::Token::FixedBytes(bytes) => {
            if bytes.len() != 32 {
                return Err(Error::FixedBytesDecodingError(bytes.len()).into());
            }

            let mut claim_commitment = [0u8; 32];
            claim_commitment.copy_from_slice(&bytes);
            claim_commitment
        }
        token => {
            return Err(Error::ExpectedFixedBytes(token).into());
        }
    };
    let refund_commitment = match tokens.remove(0) {
        ethers::abi::Token::FixedBytes(bytes) => {
            if bytes.len() != 32 {
                return Err(Error::FixedBytesDecodingError(bytes.len()).into());
            }

            let mut refund_commitment = [0u8; 32];
            refund_commitment.copy_from_slice(&bytes);
            refund_commitment
        }
        token => {
            return Err(Error::ExpectedFixedBytes(token).into());
        }
    };

    let claimer: Address = match tokens.remove(0) {
        ethers::abi::Token::Address(bytes) => bytes.into(),
        token => {
            return Err(Error::ExpectedAddress(token).into());
        }
    };

    let (timeout_1, timeout_2) = match (tokens.remove(0), tokens.remove(0)) {
        // tokens index 3 and 4
        (ethers::abi::Token::Uint(timeout_1), ethers::abi::Token::Uint(timeout_2)) => {
            (timeout_1, timeout_2)
        }
        _ => {
            return Err(Error::ExpectedTwoU256s.into());
        }
    };

    let asset: Address = match tokens.remove(0) {
        ethers::abi::Token::Address(bytes) => bytes.into(),
        token => {
            return Err(Error::ExpectedAddress(token).into());
        }
    };

    let value = match tokens.remove(0) {
        ethers::abi::Token::Uint(value) => value,
        token => {
            return Err(Error::ExpectedU256(token).into());
        }
    };

    let nonce = match tokens.remove(0) {
        ethers::abi::Token::Uint(nonce) => nonce,
        token => {
            return Err(Error::ExpectedU256(token).into());
        }
    };

    let contract_swap = Swap {
        owner: receipt.from,
        claim_commitment,
        refund_commitment,
        claimer,
        timeout_1,
        timeout_2,
        asset,
        value,
        nonce,
    };
    Ok((contract_swap, swap_id))
}

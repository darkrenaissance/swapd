use super::{traits::ContractSwapArgs, Error};
use crate::protocol::traits::Follower;
use smol::channel;

use log::{info, warn};

#[allow(dead_code)]
pub(crate) enum Event {
    // occurs when the counterparty has locked funds in the contract.
    // contains the swap id within the contract.
    CounterpartyFundsLocked(ContractSwapArgs),
    ReadyToClaim,
    CounterpartyFundsRefunded([u8; 32]),
}

#[allow(dead_code)]
#[derive(Debug, Clone, PartialEq, Eq)]
enum State {
    WaitingForCounterpartyFundsLocked,
    WaitingForContractReady,
    Completed,
}

#[allow(dead_code)]
struct Swap {
    // the chain-specific event handler
    handler: Box<dyn Follower + Send + Sync>,

    // the event receiver channel for the swap
    // the [`Watcher`] sends events to this channel
    event_rx: channel::Receiver<Event>,

    // the current state of the swap
    state_tx: async_watch::Sender<State>,
    state_rx: async_watch::Receiver<State>,
}

#[allow(dead_code)]
impl Swap {
    fn new(
        handler: Box<dyn Follower + Send + Sync>,
        event_rx: channel::Receiver<Event>,
    ) -> (Self, async_watch::Receiver<State>) {
        let state = async_watch::channel(State::WaitingForCounterpartyFundsLocked);
        (Self { handler, event_rx, state_tx: state.0, state_rx: state.1.clone() }, state.1)
    }

    async fn run(&mut self) -> Result<(), crate::Error> {
        loop {
            match self.event_rx.recv().await {
                Ok(Event::CounterpartyFundsLocked(contract_swap_args)) => {
                    info!("counterparty funds locked");

                    if !matches!(*self.state_rx.borrow(), State::WaitingForCounterpartyFundsLocked)
                    {
                        warn!(
                            "unexpected event CounterpartyFundsLocked, state is {:?}",
                            *self.state_rx.borrow()
                        );
                        return Err(Error::UnexpectedCounterpartyFundsLocked.into());
                    }

                    self.handler.handle_counterparty_funds_locked(contract_swap_args)?;

                    self.state_tx
                        .send(State::WaitingForContractReady)
                        .expect("state channel should not be dropped");
                }
                Ok(Event::ReadyToClaim) => {
                    info!("ready to claim funds on counterparty chain");

                    if !matches!(*self.state_rx.borrow(), State::WaitingForContractReady) {
                        warn!(
                            "unexpected event ReadyToClaim, state is {:?}",
                            *self.state_rx.borrow()
                        );
                        return Err(Error::UnexpectedReadyToClaim.into());
                    }

                    self.handler.handle_ready_to_claim()?;
                    self.state_tx
                        .send(State::Completed)
                        .expect("state channel should not be dropped");
                }
                Ok(Event::CounterpartyFundsRefunded(counterparty_secret)) => {
                    info!("counterparty refunded funds");
                    if !matches!(*self.state_rx.borrow(), State::WaitingForContractReady) {
                        warn!(
                            "unexpected event CounterpartyFundsRefunded, state is {:?}",
                            *self.state_rx.borrow()
                        );
                        return Err(Error::UnexpectedCounterpartyFundsRefunded.into());
                    }

                    self.handler.handle_counterparty_funds_refunded(counterparty_secret)?;
                    self.state_tx
                        .send(State::Completed)
                        .expect("state channel should not be dropped");
                }
                Err(_) => {
                    info!("event channel closed, exiting");
                    break;
                }
            }
        }

        Ok(())
    }
}
